
@HOMEOFFICE

Feature: Given that am on dvla home page and am able to verify car registration details

  Scenario Outline: As a user From the dvla web page am able to supply car registration details

    Given That from dvla web page
    Then Am presented with vehicle details page
    And  As a user am able to supply a car registration "<PlateNumber>" details
    When From the vehicle details page am able validate "<text>" car registration number

    Examples:
      | PlateNumber   |  text                                      |
      |    FL07UHO    | Check if a vehicle is taxed and has an MOT |
