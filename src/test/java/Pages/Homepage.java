package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 09/06/2017.
 */
public class Homepage {

    public WebDriver driver;


    public Homepage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);


    }
    @FindBy(how = How.CLASS_NAME,using = "button")
    public static WebElement StartNow;



    public void Searchdetails (){
        StartNow.click();


    }
}
