package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 09/06/2017.
 */
public class Numberplatepage {


    public WebDriver driver;

    public Numberplatepage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }


    @FindBy(how = How.CSS, using = "#Vrm")public static WebElement RegNumber1;
    @FindBy(how = How.NAME,using = "Continue")public static WebElement Continue;



    public void CarNumberPlate(String Num){
        RegNumber1.sendKeys(Num);
        Continue.click();

    }



}
