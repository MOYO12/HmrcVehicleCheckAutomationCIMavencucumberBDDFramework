package dvla;

import Base.Baseclass;
import Pages.Carconfirmation;
import Pages.Carverifydetailspage;
import Pages.Homepage;
import Pages.Numberplatepage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

/**
 * Created by User on 09/06/2017.
 */
public class Stepdefinition extends Baseclass {

    public static WebDriver driver;

              public Stepdefinition(){
                  this.driver = Hook.driver;
              }


    @Given("^That from dvla web page$")
    public void that_from_dvla_web_page() throws Throwable {
        driver.get("https://www.gov.uk/get-vehicle-information-from-dvla");
        Homepage HMP = new Homepage(driver);
        HMP.Searchdetails();
    }

    @Then("^Am presented with vehicle details page$")
    public void am_presented_with_vehicle_details_page() throws Throwable {
        Carverifydetailspage CVD = new Carverifydetailspage(driver);

    }

    @And("^As a user am able to supply a car registration \"([^\"]*)\" details$")
    public void as_a_user_am_able_to_supply_a_car_registration_details(String Num) throws Throwable {
        Numberplatepage NP = new Numberplatepage(driver);
        Carverifydetailspage CVD = new Carverifydetailspage(driver);
        NP.CarNumberPlate(Num);
    }

    @When("^From the vehicle details page am able validate \"([^\"]*)\" car registration number$")
    public void from_the_vehicle_details_page_am_able_validate_car_registration_number(String text) throws Throwable {
        Carconfirmation CAR = new Carconfirmation(driver);
        Carverifydetailspage CVD = new Carverifydetailspage(driver);
        CVD.ValidateDetails(text);
        CAR.Access();

    }


}
