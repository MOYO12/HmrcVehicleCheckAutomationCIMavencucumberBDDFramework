package dvla;

import cucumber.api.java.Before;
import org.junit.After;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by User on 09/06/2017.
 */
public class Hook {



    public static WebDriver driver;

    @Before
    public static WebDriver setup(){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\Documents\\ZLATAN\\ChromeDriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        System.out.println("The page title is "+ driver.getTitle());
        return driver;
    }

      @cucumber.api.java.After
      public void teardown()
      {
          driver.quit();
     }



}
