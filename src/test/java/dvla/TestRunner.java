package dvla;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by User on 09/06/2017.
 */


@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources",
        plugin = {"pretty","html:target/cucumber", "json:target/cucumber/report.json"},
        tags = "@HOMEOFFICE",
        monochrome = false
)





public class TestRunner {
}
