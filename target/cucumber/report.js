$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("vehiclecheck.feature");
formatter.feature({
  "line": 4,
  "name": "Given that am on dvla home page and am able to verify car registration details",
  "description": "",
  "id": "given-that-am-on-dvla-home-page-and-am-able-to-verify-car-registration-details",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@HOMEOFFICE"
    }
  ]
});
formatter.scenarioOutline({
  "line": 6,
  "name": "As a user From the dvla web page am able to supply car registration details",
  "description": "",
  "id": "given-that-am-on-dvla-home-page-and-am-able-to-verify-car-registration-details;as-a-user-from-the-dvla-web-page-am-able-to-supply-car-registration-details",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "That from dvla web page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "Am presented with vehicle details page",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "As a user am able to supply a car registration \"\u003cPlateNumber\u003e\" details",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "From the vehicle details page am able validate \"\u003ctext\u003e\" car registration number",
  "keyword": "When "
});
formatter.examples({
  "line": 13,
  "name": "",
  "description": "",
  "id": "given-that-am-on-dvla-home-page-and-am-able-to-verify-car-registration-details;as-a-user-from-the-dvla-web-page-am-able-to-supply-car-registration-details;",
  "rows": [
    {
      "cells": [
        "PlateNumber",
        "text"
      ],
      "line": 14,
      "id": "given-that-am-on-dvla-home-page-and-am-able-to-verify-car-registration-details;as-a-user-from-the-dvla-web-page-am-able-to-supply-car-registration-details;;1"
    },
    {
      "cells": [
        "FL07UHO",
        "Check if a vehicle is taxed and has an MOT"
      ],
      "line": 15,
      "id": "given-that-am-on-dvla-home-page-and-am-able-to-verify-car-registration-details;as-a-user-from-the-dvla-web-page-am-able-to-supply-car-registration-details;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 7128627282,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "As a user From the dvla web page am able to supply car registration details",
  "description": "",
  "id": "given-that-am-on-dvla-home-page-and-am-able-to-verify-car-registration-details;as-a-user-from-the-dvla-web-page-am-able-to-supply-car-registration-details;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 2,
      "name": "@HOMEOFFICE"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "That from dvla web page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "Am presented with vehicle details page",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "As a user am able to supply a car registration \"FL07UHO\" details",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "From the vehicle details page am able validate \"Check if a vehicle is taxed and has an MOT\" car registration number",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.match({
  "location": "Stepdefinition.that_from_dvla_web_page()"
});
formatter.result({
  "duration": 6617908152,
  "status": "passed"
});
formatter.match({
  "location": "Stepdefinition.am_presented_with_vehicle_details_page()"
});
formatter.result({
  "duration": 187590,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "FL07UHO",
      "offset": 48
    }
  ],
  "location": "Stepdefinition.as_a_user_am_able_to_supply_a_car_registration_details(String)"
});
formatter.result({
  "duration": 948008555,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Check if a vehicle is taxed and has an MOT",
      "offset": 48
    }
  ],
  "location": "Stepdefinition.from_the_vehicle_details_page_am_able_validate_car_registration_number(String)"
});
formatter.result({
  "duration": 715875055,
  "status": "passed"
});
formatter.after({
  "duration": 1970884813,
  "status": "passed"
});
});